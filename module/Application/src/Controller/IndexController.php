<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Component\StringSplitter;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }

    public function testOneAction()
    {
        return ['test' => 'Test page'];
    }

    public function testTwoAction()
    {
        $str = 'Congratulations, @user! You ha-ve successful\'ly installed the #ZF Skele_ton "Application"
            to 100%.
            This skeleton: can serve as a (simple) [starting] {point} for     //   you to begin
            building your application on ZF.
            А теперь ТАКЖЕ протестируем стро-ку на русском! языке!!!';

        $splitter = new StringSplitter();
        $words = $splitter($str);

        return compact('str', 'words');
    }

}
