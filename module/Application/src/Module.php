<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\MvcEvent;

class Module
{
    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event)
    {
        $app = $event->getApplication();
        $app->getEventManager()->attach(MvcEvent::EVENT_DISPATCH, function ($e) {
            $lang = $e->getRequest()->getQuery('lang');

            if ($lang) {
                $this->serviceManager = $e->getApplication()->getServiceManager();
                $translator = $this->serviceManager->get('MvcTranslator');
                $translator->setLocale($lang);
            }
        });
    }
}
