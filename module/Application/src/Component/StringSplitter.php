<?php
namespace Application\Component;

class StringSplitter
{
    public function __invoke($str)
    {
        return preg_split('/(\W?\s\W?)|(\W+$)/u', str_replace('&', 'and', $str), null, PREG_SPLIT_NO_EMPTY);
    }
}
