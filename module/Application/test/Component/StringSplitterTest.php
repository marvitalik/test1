<?php
namespace Application\test;

use PHPUnit\Framework\TestCase;
use Application\Component\StringSplitter;

class StringSplitterTest extends TestCase
{
    public function testSplit()
    {
        $model = new StringSplitter();
        $this->assertTrue(is_callable($model));

        $result = $model('So\'me "test" stri`ng that     has 8 words & (one) di-gi-t!!!');
        $this->assertTrue(is_array($result));
        $this->assertCount(10, $result);
        $this->assertArraySubset(['So\'me', 'test', 'stri`ng', 'that', 'has', '8', 'words', 'and', 'one', 'di-gi-t'], $result);

        $result2 = $model('И по-русски');
        $this->assertTrue(is_array($result2));
        $this->assertCount(2, $result2);
        $this->assertArraySubset(['И', 'по-русски'], $result2);

    }
}
